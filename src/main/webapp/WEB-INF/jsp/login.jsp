<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="common/header.jsp" %>
<!DOCTYPE >
<html lang="zh-CN" class="no-js">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>login</title>
<link rel="stylesheet" type="text/css" href="${ctx}/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/css/demo.css" />
<!--必要样式-->
<link rel="stylesheet" type="text/css" href="${ctx}/css/component.css" />
<script type="text/javascript" src="${ctx}/js/jquery-1.9.1.min.js"></script>
<script>
	$(function(){
		$("#loginBtn").click(function(){
			$.ajax({
			    url:"${ctx}/login/toLogin",   //请求的url地址
			    dataType:"json",   //返回格式为json
			    async:true,//请求是否异步，默认为异步，这也是ajax重要特性
			    data:{"userName":$("#logname").val(),"password":$("#logpass").val()},    //参数值
			    type:"GET",   //请求方式
			    success:function(result){
			        var data=result;
			        if(data.code==200){
			        	window.location.href="${ctx}/user/showAllUser"
			        };
			        if(data.code==400){
			        	alert("登录失败");
			        }
			    },
			    error:function(){
			        //请求出错处理
			    }
			});
		});
	});
</script>
</head>
<body>
		<div class="container demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="logo_box">
						<h3>欢迎你</h3>
						<form action="#"  name="f" method="post">
							<div class="input_outer">
								<span class="u_user"></span>
								<input name="logname" class="text" style="position: absolute;z-index: -1;" disabled   autocomplete = "off" style="color: #FFFFFF !important" type="text">
								<input name="logname" id="logname" class="text"   autocomplete = "off" style="color: #FFFFFF !important" type="text" placeholder="请输入账户">
							</div>
							<div class="input_outer">
								<span class="us_uer"></span>
								<input name="logpass" class="text" style="position: absolute;z-index: -1;" disabled  autocomplete = "off" style="color: #FFFFFF !important; position:absolute;z-index:100;"value=""  type="password" >
								<input name="logpass" id="logpass" class="text"  autocomplete = "off" style="color: #FFFFFF !important; position:absolute;z-index:100;" value=""  type="password" placeholder="请输入密码">
							</div>
							<div class="mb2"><a href="javascript:void(0);" id="loginBtn" class="act-but submit" style="color: #FFFFFF">登录</a></div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /container -->
		<script src="${ctx}/js/TweenLite.min.js"></script>
		<script src="${ctx}/js/EasePack.min.js"></script>
		<script src="${ctx}/js/rAF.js"></script>
		<script src="${ctx}/js/demo-1.js"></script>
	</body>
</html>