package com.cn.hnust.activemq;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
//消费者(主题模式)
public class TopicConsumer {
	
	private static final String url="tcp://39.106.149.133:61616";
	private static final String topicName="topic-test";
	
	public static void main(String[] args) throws JMSException {
		//创建ConnectionFactory
		 ConnectionFactory connectionFactory=new ActiveMQConnectionFactory(url);
		 //创建连接
		 Connection connection=connectionFactory.createConnection();
		 //启动连接
		 connection.start();
		 //创建会话
		 Session session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		 //创建一个目标
		 Destination destination=session.createTopic(topicName);
		 //创建一个监听器
		 //创建一个消费者
		 MessageConsumer consumer=session.createConsumer(destination);
		 consumer.setMessageListener(new MessageListener() {
			@Override
			public void onMessage(Message message) {
				TextMessage textMessage=(TextMessage) message;
				try {
					System.out.println("接受消息"+textMessage.getText());
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		 });
		 //关闭连接
//		 connection.close();
	}

}
