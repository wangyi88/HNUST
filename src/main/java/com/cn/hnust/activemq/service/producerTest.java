package com.cn.hnust.activemq.service;

import javax.jms.Destination;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class producerTest {
	public static void main(String[] args) {
		 // 加载spring
		ClassPathXmlApplicationContext springContext = SpringContainer.getSpringContext();
		ProducerService producerService = (ProducerService)springContext.getBean("producerService");
	    ConsumerService consumerService = (ConsumerService)springContext.getBean("consumerService");
	    Destination destination = (Destination)springContext.getBean("queueDestination");
	    producerService.sendMessage("测试消息队列");
	    consumerService.receive(destination);
	}
}
