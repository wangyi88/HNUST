package com.cn.hnust.activemq.service;

import javax.jms.Destination;

import org.springframework.stereotype.Service;


public interface ProducerService {
	  /**
     * 向指定队列发送消息
     */
     void sendMessage(Destination destination, final String msg);
     /**
     * 向默认队列发送消息
     */
     void sendMessage(final String msg);
}
