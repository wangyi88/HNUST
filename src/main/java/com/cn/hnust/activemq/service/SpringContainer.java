package com.cn.hnust.activemq.service;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringContainer {
	
		public static final String DEFAULT_SPRING_CONFIG = "classpath:*.xml";

	    public static ClassPathXmlApplicationContext getSpringContext() {
	        return MySpringContainer.context;
	    }

	    private static class MySpringContainer{
	        private static ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(DEFAULT_SPRING_CONFIG.split("[,\\s]+"));
	    }
}
