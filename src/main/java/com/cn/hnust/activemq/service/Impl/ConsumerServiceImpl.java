package com.cn.hnust.activemq.service.Impl;

import javax.annotation.Resource;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.cn.hnust.activemq.service.ConsumerService;

@Service("consumerService")
public class ConsumerServiceImpl implements ConsumerService{
	
	@Resource(name="jmsTemplate")
    private JmsTemplate jmsTemplate;

	@Override
	 /**
     * 接受消息
     */
    public void receive(Destination destination) {
        TextMessage tm = (TextMessage) jmsTemplate.receive(destination);
        try {
            System.out.println("从队列" + destination.toString() + "收到了消息：\t"
                    + tm.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
