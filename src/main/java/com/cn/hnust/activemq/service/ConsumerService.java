package com.cn.hnust.activemq.service;

import javax.jms.Destination;

import org.springframework.stereotype.Service;

public interface ConsumerService {
	
	 /**
     * 接受消息
     */
    void receive(Destination destination);
}
