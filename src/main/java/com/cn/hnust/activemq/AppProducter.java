package com.cn.hnust.activemq;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
//生产者
public class AppProducter {
	
	private static final String url="tcp://39.106.149.133:61616";
	private static final String queue="queue-test";
	public static void main(String[] args) throws JMSException {
		 //创建ConnectionFactory
		 ConnectionFactory connectionFactory=new ActiveMQConnectionFactory(url);
		 //创建连接
		 Connection connection=connectionFactory.createConnection();
		 //启动连接
		 connection.start();
		 //创建会话
		 Session session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		 //创建一个目标
		 Destination destination=session.createQueue(queue);
		 //创建一个生产者
		 MessageProducer Producer=session.createProducer(destination);
		 for (int i = 0; i < 100; i++) {
			//创建消息
			 TextMessage textMessage=session.createTextMessage("test"+i);
			 //发布消息
			 Producer.send(textMessage);
			 System.out.println("发送消息"+textMessage.getText());
		 }
		 //关闭连接
		 connection.stop();
	}
}
