package com.cn.hnust.util;

import java.io.IOException;
import java.util.Properties;

public class AppPropertiesUtil {
	    private static Properties p = new Properties();  
	    static{  
	        try {  
	            p.load(AppPropertiesUtil.class.getClassLoader().getResourceAsStream("app.properties"));  
	        } catch (IOException e) {  
	            e.printStackTrace();   
	        }  
	    }  
	    public static String getValue(String key){  
	        return p.getProperty(key);  
	    }  
}
