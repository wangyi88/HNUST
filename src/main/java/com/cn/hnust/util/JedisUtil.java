package com.cn.hnust.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Jedis基础类
 * @author wy
 *
 */
public class JedisUtil {

	private static JedisUtil instance = new JedisUtil();

	private JedisPool pool;

	private JedisUtil() {
		try {	
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxActive(Integer.valueOf("100"));
			config.setMaxIdle(Integer.valueOf("100"));
			//最小空闲数
			config.setMinIdle(Integer.valueOf("50")); 
			//如果为true，则得到的jedis实例均是可用的;
			config.setTestOnBorrow(true); 
//			pool = new JedisPool(config, "39.106.149.133", Integer.valueOf("6379"));
			//这是要密码的配置
			pool = new JedisPool(config, "39.106.149.133", Integer.valueOf("6377"), Integer.valueOf("400"), "nKze>3Fd8");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * get connection from pool
	 * 获取连接
	 * @return
	 */
	private Jedis getConn() {
		return pool.getResource();
	}

	/**
	 * shutdown connect
	 * 关闭连接
	 * @param conn
	 */
	private void shutdown(Jedis conn) {
		pool.returnResource(conn);
	}

	public static JedisUtil getInstance() {
		return instance;
	}

	public static String getValue(String table, String key) {
		table = table + "#";
//		 return JedisUtil.getInstance().pool.getResource().get(table + key );
		Jedis conn = instance.getConn();
		String value = conn.get(table + key);
		instance.shutdown(conn);
		return value;

	}

	public static String setValue(String table, String key, String value) {
		table = table + "#";
		Jedis conn = instance.getConn();
		String ret = conn.set(table + key, value);
		instance.shutdown(conn);
		return ret;
	}

	// 单位：秒
	public static String setValueTime(String table, String key, String value, int seconds) {
		table = table + "#";
		Jedis conn = instance.getConn();
		String ret = conn.setex(table + key, seconds, value);
		instance.shutdown(conn);
		return ret;
	}

	public static void delValue(String table, String key) {
		table = table + "#";
		Jedis conn = instance.getConn();
		conn.del(table + key);
		instance.shutdown(conn);

	}
	
	//放字节
	public static String setByteValue(byte[] key,byte[] value) {
		Jedis conn = instance.getConn();
		String ret = conn.set(key, value);
		instance.shutdown(conn);
		return ret;
	}
	
	//取字节
	public static byte[] getByteValue(byte[] key) {
		Jedis conn = instance.getConn();
		byte[] value1 = conn.get(key);
		instance.shutdown(conn);
		return value1;
	}

}
