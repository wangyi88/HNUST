package com.cn.hnust.common;

import java.security.GeneralSecurityException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cn.hnust.util.MyException;
import com.cn.hnust.util.ResultBean;
import com.cn.hnust.util.SendMailMessage;
import com.sun.xml.internal.ws.api.model.CheckedException;

/**
 * aop处理的类
 * @author 0
 *
 */
public class ControllerAOP {
	private static final Logger logger = LoggerFactory.getLogger(ControllerAOP.class);
	
	public Object handlerControllerMethod(ProceedingJoinPoint pjp) {
		long startTime = System.currentTimeMillis();
		ResultBean<?> result;
		try {
			result = (ResultBean<?>) pjp.proceed();
			logger.info(pjp.getSignature() + "use time:"
					+ (System.currentTimeMillis() - startTime));
		} catch (Throwable e) {
			result = handlerException(pjp, e);
		}
		return result;
	}
	
	/**
	* 这是异常处理的逻辑
	*/
	private ResultBean<?> handlerException(ProceedingJoinPoint pjp, Throwable e) {
		
		@SuppressWarnings("rawtypes")
		ResultBean<?> result = new ResultBean();
		if (e instanceof CheckedException) {
			result.setMsg(e.getLocalizedMessage());
			result.setCode(ResultBean.FAIL);
		}else if(e instanceof MyException){
			result.setMsg(e.getMessage());
			result.setCode(((MyException) e).getErrorCode());
		}else {
			logger.error(pjp.getSignature() + " error ", e);
			result.setMsg(e.toString());
			result.setCode(ResultBean.FAIL);
			try {
				SendMailMessage.sendMailMessage("1023563646@qq.com", "错误异常", e.toString());
			} catch (GeneralSecurityException e1) {
				e1.printStackTrace();
			}
		}
		return result;
	}
}
