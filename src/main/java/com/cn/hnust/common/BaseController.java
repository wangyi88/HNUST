package com.cn.hnust.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

public class BaseController {
	
	private static Logger logger = LogManager.getLogger(BaseController.class);
	
	public static String OPENID = "session_openid" ;
	
	//这是分割线
	protected HttpServletRequest request;  
	protected HttpServletResponse response;  
	protected HttpSession session;
	protected Model model;
	
	protected String SESSION_OPENID ;      //用户openid
	protected String SESSION_OPENIDCODE ;  //openidcode
	protected String SESSION_SOURCE="PC" ;      //source
	protected String SESSION_SOURCEOID ;   //分享者ID
	
    @ModelAttribute
    public void setReqAndRes(
    		HttpServletRequest request, HttpServletResponse response,Model model){  
        this.request = request;
        this.response = response;
        this.session = request.getSession();
        this.model = model ;
        logger.info("进入controller");
        model.addAttribute("baseroot",this.getBasePath());
        //获取cookie里的值
        Cookie[] cookies = request.getCookies() ;
		if( cookies!= null ){
	        for(Cookie c :cookies ){
	        	if( "openid".equals(c.getName()) ){
	        		SESSION_OPENID = c.getValue() ;
	        		continue ;
	        	}
	        	if( "openidcode".equals(c.getName()) ){
	        		SESSION_OPENIDCODE = c.getValue() ;
	        		continue ;
	        	}
	        	if( "source".equals(c.getName()) ){
	        		SESSION_SOURCE = c.getValue() ;
	        		continue ;
	        	}
	        	if( "sourceoid".equals(c.getName()) ){
	        		SESSION_SOURCEOID = c.getValue() ;
	        		continue ;
	        	}
	        }
		}
    }
    
	protected String getBasePath(){
		String path = this.request.getContextPath();
		int port= this.request.getServerPort();
		
		String basePath = this.request.getScheme() + "://"
				+ this.request.getServerName() +(port==80?"":( ":" + this.request.getServerPort()))
				+ path;
		return basePath;
	}
	
	
	 /**
     * 获取SESSION_ID
     * */
    public String getSESSION_OPENID( HttpServletRequest request ){
    	HttpSession sesssion = request.getSession();
    	return (String)sesssion.getAttribute( BaseController.OPENID );
    }
    
    public String createLinkString(Map<String, String> params) {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = "";

        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);

            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                prestr = prestr + key + "=" + value;
            } else {
                prestr = prestr + key + "=" + value + "&";
            }
        }

        return prestr;
    }
}