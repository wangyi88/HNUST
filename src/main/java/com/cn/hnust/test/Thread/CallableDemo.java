//package com.cn.hnust.test.Thread;
//
//import java.util.concurrent.Callable;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.Future;
//import java.util.concurrent.FutureTask;
//
//class CallableTemp implements Callable<String>{
//	@Override
//	public String call() throws Exception {
//		for (int i = 0; i < 20; i++) {
//			System.out.println("卖票X="+i);
//		}
//		return "票卖完了，下次吧";
//	}
//}
//public class CallableDemo {
//	public static void main(String[] args) throws InterruptedException, ExecutionException {
//		CallableTemp m1=new CallableTemp();
//		FutureTask<String> task=new FutureTask<>(m1);
//		new Thread(task).start();
//		System.out.println(task.get());
//	}
//}
