package com.cn.hnust.test.Thread;


class ThreadTemp extends Thread{
	private int ticket=10; //一共十张票
	@Override
	public void run() {
		long time= System.currentTimeMillis();
		for (int i = 0; i < 20; i++) {
			if(this.ticket>0){
				System.out.println("票卖出去了 ticket还剩"+this.ticket--);
			}
		}
		System.out.println(System.currentTimeMillis()-time);
	}
}
//本身ThreadTemp类继承了Thread类，本身就有start方法，结果还需要再去调用Thread接口的start方法才能实现若干线程进行同一数据的处理操作
public class DifferenceThread{
	public static void main(String[] args) {
		ThreadTemp mt=new ThreadTemp();
		new Thread(mt).start();
		new Thread(mt).start();
		new Thread(mt).start();
	}
}
