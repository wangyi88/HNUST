package com.cn.hnust.test.Thread;
/**
 * 为了描述多线程的同步问题
 * @author 0
 *
 */
//这是通过同步代码块来实现同步
class TickeTemp implements Runnable{
	private int ticket=10;//要卖票的总数
	@Override
	public void run() {
		for (int i = 0; i < 20; i++) {
			//在同一时刻,只允许一个线程进入并且操作，其他线程需要等待
			synchronized (this){//表示程序逻辑上锁
				if(this.ticket>0){//表示现在还有票
					try {
						Thread.sleep(200);//模拟网络延迟
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()+"卖票，剩余的票数为"+this.ticket--);
				}
			}
		}
	}
}
//这是同步方法实现的线程同步
class Ticketask implements Runnable{
	private int ticket=10;//要卖票的总数
	@Override
	public void run() {
		for (int i = 0; i < 20; i++) {
			this.sale();
		}
	}
	public synchronized void sale(){
		if(this.ticket>0){//表示现在还有票
			try {
				Thread.sleep(200);//模拟网络延迟
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName()+"卖票，剩余的票数为"+this.ticket--);
		}
	}
}
public class TicketDemo {
	public static void main(String[] args) {
		Ticketask t1=new Ticketask();
		new Thread(t1,"卖票人1").start();
		new Thread(t1,"卖票人2").start();
		new Thread(t1,"卖票人3").start();
	}
}
