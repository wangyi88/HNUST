package com.cn.hnust.test.Thread;

class MyThread extends Thread{//是一个线程的主类
	private String title;
	public MyThread(String title){
		this.title=title;
	}
	@Override
	public void run() { //所有的线程从此处开始执行
		for (int i = 0; i < 10; i++) {
			System.out.println(this.title+".i="+i);
		}
	}
}
public class ThreadDemo {
	public static void main(String[] args) {
		MyThread m1=new MyThread("线程A");
		MyThread m2=new MyThread("线程b");
		MyThread m3=new MyThread("线程c");
		m1.start();
		m2.start();
		m3.start();
	}
}
