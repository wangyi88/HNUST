package com.cn.hnust.test.Thread;
//生产者
class DataProvider implements Runnable{
	private Data data;
	public DataProvider(Data data){
		this.data=data;
	}
	@Override
	public void run() {
		for (int i = 0; i < 50; i++) {
			if(i%2==0){
				this.data.set("老李","是个好人");
			}else{
				this.data.set("败类","是个坏人");
			}
		}
	}
}
//消费者
class DataConsumer implements Runnable{
	private Data data;
	public DataConsumer(Data data){
		this.data=data;
	}
	@Override
	public void run() {
		for (int i = 0; i < 50; i++) {
			this.data.get();
		}
	}
}
//负责数据保存
class Data{
	private String title;
	private String note;
	//flag=true 表示允许生产，但是不允许消费者取走
	//flag=false 表示允许消费者取走，但是不允许生产者生产
	private boolean flag=false;
	public synchronized void get(){
		if(flag==false){//已经生产了，不允许重复生产
			try {
				super.wait();//等待执行
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(this.title+"="+this.note);
		this.flag=false;//表示生产过了不允许再生产了
		super.notify();//唤醒等待线程 
	}
	public synchronized void set(String title, String note){
		if(this.flag==true){//现在不允许取走
			try {
				super.wait();//等待执行
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.title=title;
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.note=note;
		this.flag=true;//表示继续生产
		super.notify();//唤醒等待线程
	}
}
//本方法是为了解决线程的同步和重复操作问题 运用了synchronized关键字和wait()和notify()方法
public class ProductDemo {
	public static void main(String[] args) {
		Data data=new Data();
		new Thread(new DataProvider(data)).start();
		new Thread(new DataConsumer(data)).start();
	}
}
