package com.cn.hnust.test.Thread;
class PriorityTemp implements Runnable{
	@Override
	public void run() {
		for (int i = 0; i < 2; i++) {
			System.out.println(Thread.currentThread()+"X="+i);
		}
	}
}
public class PriorityDemo {
	public static void main(String[] args) {
		PriorityTemp p1=new PriorityTemp();
		Thread t1=new Thread(p1,"线程A");
		Thread t2=new Thread(p1,"线程B");
		Thread t3=new Thread(p1,"线程C");
		t1.setPriority(Thread.MAX_PRIORITY);
		t2.setPriority(Thread.MIN_PRIORITY);
		t3.setPriority(Thread.NORM_PRIORITY);
		t1.start();
		t2.start();
		t3.start();
	}
}
