package com.cn.hnust.test.Thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

//本类是线程池的实现
public class ThreadPollDemo {
	public static void main(String[] args) throws InterruptedException {
		//下面是创建一个线程池的操作
//		onePool one=new onePool();
//		one.onePoolDemo();
		//下面是创建无数的线程池
//		MaxPool max=new MaxPool();
//		max.maxPoolDemo();
		//下面是创建固定长度的线程池
//		FixedPool fix=new FixedPool();
//		fix.fixedPoolDemo();
		//
		quatzPool quatz=new quatzPool();
		quatz.quatzPoolDemo();
	}
}
class onePool{
	public void onePoolDemo(){
		//创建了一个线程池的模型，但是里面没有线程
		ExecutorService ex1=Executors.newSingleThreadExecutor();
		for (int x = 0; x < 10; x++) {
			ex1.submit(new Runnable() {
				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName()+"x");
				}//执行线程的操作
			});
		}
		ex1.shutdown();//关闭线程池
	}
};
//无限长度的线程池
class MaxPool{
	public void maxPoolDemo() throws InterruptedException{
		//创建了一个线程池的模型，但是里面没有线程
		ExecutorService ex1=Executors.newCachedThreadPool();
		for (int x = 0; x < 10; x++) {
//			Thread.sleep();
			ex1.submit(new Runnable() {
				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName()+"x");
				}//执行线程的操作
			});
		}
		ex1.shutdown();//关闭线程池
	}
};
//固定长度的线程池
class FixedPool{
	public void fixedPoolDemo() throws InterruptedException{
		//创建了一个线程池的模型，里面固定长度是三个
		ExecutorService ex1=Executors.newFixedThreadPool(3);
		for (int x = 0; x < 10; x++) {
			ex1.submit(new Runnable() {
				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName()+"x");
				}//执行线程的操作
			});
		}
		ex1.shutdown();//关闭线程池
	}
}
class quatzPool{
	public void quatzPoolDemo() throws InterruptedException{
		//创建了一个具备一个线程大小的定时调度线程池
		ScheduledExecutorService ex3=Executors.newScheduledThreadPool(1);
		for (int x = 0; x < 2; x++) {
			synchronized (this){
				ex3.scheduleAtFixedRate(new Runnable() {
					@Override
					public void run() {
						System.out.println(Thread.currentThread().getName()+"x");
					}
				}, 3, 2, TimeUnit.SECONDS);
			}
		}
	}
};
