package com.cn.hnust.test.Thread;

class MyRunnable implements Runnable{//是一个线程的主类
	private String title;
	public MyRunnable(String title){
		this.title=title;
	}
	@Override
	public void run() { //所有的线程从此处开始执行
		for (int i = 0; i < 10; i++) {
			System.out.println(this.title+".i="+i);
		}
	}
}
public class RunnableDemo {
	public static void main(String[] args) {
		MyRunnable m1=new MyRunnable("线程A");
		MyRunnable m2=new MyRunnable("线程b");
		MyRunnable m3=new MyRunnable("线程c");
		new Thread(m1).start();
		new Thread(m2).start();
		new Thread(m3).start();
	}
}
