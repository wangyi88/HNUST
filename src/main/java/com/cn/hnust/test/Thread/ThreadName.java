package com.cn.hnust.test.Thread;

class NameDemo implements Runnable{
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(Thread.currentThread().getName());
		}
	}
	
}
public class ThreadName {
	public static void main(String[] args) {
		NameDemo n1=new NameDemo();
		n1.run();//直接通过对象调用run方法，线程名称叫main
		new Thread(n1,"王五").start();//通过线程调用
	}
}
