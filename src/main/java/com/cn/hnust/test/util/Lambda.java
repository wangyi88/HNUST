package com.cn.hnust.test.util;

/**
 * 这个类是为了展示jdk1.8的Lambda特性  函数式编程
 * @author 0
 *
 */
interface tt{
	public void print();
}
public class Lambda {
	public static void main(String[] args) {
		tt t1 = new tt() {
			@Override
			public void print() {
				System.out.println("123");
			}
		};
		t1.print();
	}
	/*
	 * 
	//面向对象的缺点是结构必须非常完整   该方法就是 函数式编程的使用
	public static void main(String[] args) {
		tt t1 =() -> System.out.println("234");
		t1.print();
	}
	*/
}
