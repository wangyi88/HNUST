package com.cn.hnust.test.util.map;
import java.util.HashMap;
import java.util.Map;
class TestUser{
	private String name;
	public  TestUser(String name){
		this.name=name;
	}
	@Override
	public String toString() {
		return "姓名:"+this.name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		TestUser other = (TestUser) obj;
		if (name == null) {
			if (other.name != null){
				return false;
			}
		} else if (!name.equals(other.name)){
			return false;
		}
		return true;
	}
}
public class MapKeyDemo {
	public static void main(String[] args) {
		Map<String, TestUser> map=new HashMap<String, TestUser>();
		map.put(new String("zs"),new TestUser("张三"));
		System.out.println(map.get(new String("zs")));
		Map<TestUser, String> map1=new HashMap<TestUser, String>();
		map1.put(new TestUser("张三"),new String("zs"));
		System.out.println(map1.get(new TestUser("张三")));
	}
}
