package com.cn.hnust.test.util;

import java.util.LinkedList;
import java.util.Queue;
public class QueueDemo {
	public static void main(String[] args) {
		Queue<String> que=new LinkedList<String>();
		que.add("B");
		que.add("A");
		que.add("D");
		System.out.println(que.poll());
		System.out.println(que.poll());
		System.out.println(que.poll());
		System.out.println(que.poll());
	}
}
