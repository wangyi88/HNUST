package com.cn.hnust.test.util.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * 通过Iterator输出Map集合
 * @author 0
 *
 */

public class HashMapDemo {

	public static void main(String[] args) {
		Map<Integer , String> map=new HashMap<Integer, String>();
		map.put(1, "得分");
		map.put(2, "速度");
		Set<Map.Entry<Integer, String>> set=map.entrySet();//将Map集合变为Set集合
		Iterator<Map.Entry<Integer, String>> iter=set.iterator();//实例化Iterator接口
		while (iter.hasNext()) {//迭代输出，取出每一个Map.Entry对象
			Map.Entry<Integer,String> me=iter.next();//取出Map.Entry
			System.out.println(me.getKey()+"|"+me.getValue());
		}
	}
}
