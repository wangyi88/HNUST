package com.cn.hnust.test.util.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * 这个类是为了展示ArrayList里面的方法的，如果使用的是Collection而不是List  就没办法用get方法了
 * @author 0
 *
 */
public class ListDemo {
	 public static void main(String[] args) {
		List<String> all=new ArrayList<String>();//此时的集合里面只允许保留String数据
		System.out.println(all.size()+","+all.isEmpty());
		all.add("Hello");
		all.add("Hello");//重复数据
		all.add("World");
		all.remove("Hello");
		System.out.println(all.size()+","+all.isEmpty());
		//判断是否字符串
		System.out.println(all.contains("ABC"));
		System.out.println(all.contains("World"));
		System.out.println(all.contains("W"));
		System.out.println(all);
		//如果不用get方法的话  操作以Object形式返回，那么就有可能需要向下转型，那么就有可能造成ClassCastException的安全隐患，此类操作在开发中尽量回避 
		Object[] object =all.toArray();//变成Object对象数组
		System.out.println(Arrays.toString(object));
	}
}
