package com.cn.hnust.test.util;

import java.util.Stack;

public class StackDemo {

	public static void main(String[] args) {
		Stack<String> all=new Stack<String>();
		all.push("a");
		all.push("c");
		all.push("b");
		System.out.println(all.pop());
		System.out.println(all.pop());
		System.out.println(all.pop());
	}
}
