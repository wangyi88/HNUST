package com.cn.hnust.test.util.map;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class TreeMapDemo {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		Map<Integer, String> map=new TreeMap<Integer, String>();
		map.put(2, "hello");
		map.put(1, "world");
		map.put(3, "nihao");
		System.out.println(map);
		//把HashMap转为线程安全的
		Map map1 = Collections.synchronizedMap(map);
		System.out.println(map1);
	}
}
