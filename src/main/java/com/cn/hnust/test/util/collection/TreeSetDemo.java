package com.cn.hnust.test.util.collection;

import java.util.Set;
import java.util.TreeSet;
//本类是为了实现TreeSet对对象队形排序做的demo,如果要实现用TreeSet对对象排序，就要在基类实现Comparable方法，重写compareTo方法
class Human implements Comparable<Human>{

	private String name; 
	private Integer age;
	public Human(String name,Integer age){
		this.name=name;
		this.age=age;
	}
	@Override
	public String toString() {
		return "name = "+this.name+" ,age="+this.age+"\n";
	}
	@Override
	public int compareTo(Human o) {
		if(this.age>o.age){
			return 1;
		}else if(this.age<o.age){
			return -1;
		}else{
			return this.name.compareTo(o.name);
		}
	}
}
@SuppressWarnings("ALL")
public class TreeSetDemo {
	//按照升序排列的模式完成的
	@SuppressWarnings("MapOrSetKeyShouldOverrideHashCodeEquals")
	public static void main(String[] args) {
		//noinspection MapOrSetKeyShouldOverrideHashCodeEquals
		@SuppressWarnings("MapOrSetKeyShouldOverrideHashCodeEquals") Set<Human> all=new TreeSet<Human>();
		all.add(new Human("王李", 10));
		all.add(new Human("张阿", 10));
		all.add(new Human("赵打", 10));
		all.add(new Human("李四", 7));
		System.out.println(all);
	}
}
