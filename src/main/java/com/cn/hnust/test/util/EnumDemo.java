package com.cn.hnust.test.util;


/**
 * 本类是为了解释枚举  很少用 类的修饰符是enum 没有方法体
 * @author 0
 * enum就是一个关键字，使用enum定义的枚举类本质上就是继承了Enum这个抽象类
 */
enum color {
	Blue,RED,GREEN;
}
public class EnumDemo {
	public static void main(String[] args) {
		System.out.println(color.Blue);
		//取得这个枚举类里面某个属性的下标和name
		System.out.println(color.Blue.ordinal()+"+"+color.Blue.name());
		System.out.println();
		//遍历这个枚举类
		for (color temp : color.values()) {
			System.out.println(temp.ordinal()+"+"+temp.name());
		}
		System.out.println(sum(1));
		System.out.println(sum(new int[]{1,2,3}));
	}
	/**
	 * 可变参数  
	 * ps: 如果是多个参数 一定要把可变参数放在最后  一个方法只能设置一个可变参数
	 * @param data
	 * @return
	 */
	 public static int sum(int ... data){
		int sum=0;
		for (int i = 0; i < data.length; i++) {
			sum+=data[i];
		}
		return sum;
	 }
}
