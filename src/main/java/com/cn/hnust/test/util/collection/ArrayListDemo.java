package com.cn.hnust.test.util.collection;

import java.util.ArrayList;
import java.util.List;

class Person{
	private String name;
	private Integer age;
	public Person (String name,Integer age){
		this.name=name;
		this.age=age;
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj){
			return true;
		};
		if(this==null){
			return false;
		}
		if(!(obj instanceof Person)){
			return false;
		}
		Person per =(Person)obj;//这个地方可以向下转型
		return this.name.equals(per.name)&&this.age.equals(per.age);
	}
 	@Override
	public String toString(){
		return "Person [name="+name+",age="+age+"]";
	}
}
public class ArrayListDemo {
	public static void main(String[] args) {
		List<Person> all=new ArrayList<Person>();
		all.add(new Person("张三",10));
		all.add(new Person("李四",12));
		all.add(new Person("王五",16));
		all.remove(new Person("张三", 10));
		System.out.println(all.contains(new Person("李四", 12)));
		//对于集合中的remove(),conttains()来说必须要有equals()的支持，需要在类中重写equals()
		for (int i = 0; i < all.size(); i++) {
			System.out.println(all.get(i));
		}
		//比较两个数字的大小 返回大的
		System.out.println(Math.max(10, 2));
	}

}
