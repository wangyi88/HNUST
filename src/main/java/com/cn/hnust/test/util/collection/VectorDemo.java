package com.cn.hnust.test.util.collection;

import java.util.Enumeration;
import java.util.Vector;
/**
 * 本类是为了描述Enumeration实现的
 * @author 0
 */
public class VectorDemo {
	
	public static void main(String[] args) {
		Vector<String> all=new Vector<String>();
		all.add("Hello");
		all.add("Hello");
		all.add("World");
		Enumeration<String> temp = all.elements();
		while (temp.hasMoreElements()) {
			System.out.println(temp.nextElement());
 		}
	}
}
