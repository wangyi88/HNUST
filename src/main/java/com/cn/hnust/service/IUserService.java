package com.cn.hnust.service;

import java.util.List;

import com.cn.hnust.pojo.User;

public interface IUserService {
	public User getUserById(Integer userId);
	
	public int updateUserByUser(User user);
	
	public List<User> selectAll();
	
	public User selectByNamePass(User record);
}
