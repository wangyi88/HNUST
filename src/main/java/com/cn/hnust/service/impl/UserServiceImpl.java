package com.cn.hnust.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cn.hnust.dao.IUserDao;
import com.cn.hnust.pojo.User;
import com.cn.hnust.service.IUserService;
import com.cn.hnust.util.MyException;

@Service("userService")
public class UserServiceImpl implements IUserService {
	
	private static Logger logger=LoggerFactory.getLogger(IUserService.class);
	
	@Resource
	private IUserDao userDao;

	@Override
	public User getUserById(Integer userId) {
		logger.info("userId是"+userId);
		if(userId==null){
			throw new MyException(-2,"参数为空");
		}else{
			return this.userDao.selectByPrimaryKey(userId);
		}
	}


	@Override
	public int updateUserByUser(User user) {
		return this.userDao.updateByPrimaryKey(user);
	}



	@Override
	public List<User> selectAll() {
		return this.userDao.selectAll();
	}



	@Override
	public User selectByNamePass(User record) {
		
		return this.userDao.selectByNamePass(record);
	}

}
