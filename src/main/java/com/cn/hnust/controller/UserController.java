package com.cn.hnust.controller;

import java.io.OutputStream;
import java.util.Collection;

import javax.annotation.Resource;
import javax.jms.Destination;
import javax.servlet.http.HttpServletRequest;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cn.hnust.activemq.service.ConsumerService;
import com.cn.hnust.activemq.service.ProducerService;
import com.cn.hnust.activemq.service.SpringContainer;
import com.cn.hnust.common.BaseController;
import com.cn.hnust.pojo.User;
import com.cn.hnust.service.IUserService;
import com.cn.hnust.util.JedisUtil;
import com.cn.hnust.util.ResultBean;

/**
 * 用户信息控制层
 * @author 0
 *
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {
	
	private static Logger logger=LoggerFactory.getLogger(UserController.class);
	
	@Resource
	private IUserService userService;
	
	
	@Autowired
	private ProducerService producerService;
	
	@Autowired
	private ConsumerService consumerService;
	
	
	@RequestMapping("/toIndex")
	public String toIndex(HttpServletRequest request,Model model){
		logger.info("=================到达首页============");
		return "/login";
	}
	
	
	/**
	 * 查询所有用户信息	
	 * @return
	 */
	@RequestMapping("/showAllUser")
	@ResponseBody
	public ResultBean<Collection<User>> showAllUser(){
		return new ResultBean<Collection<User>>(userService.selectAll());
	}
	
	
	/**
	 * 根据ID查询用户信息
	 * @param id
	 * @return
	 */
	@RequestMapping("/showUser")
	@ResponseBody
	public ResultBean<User> toShowuser(@RequestParam("id")Integer id){
		return new ResultBean<User>(userService.getUserById(id));
	}
	
	/**
	 * 导出文档
	 * @param userId
	 */
	@RequestMapping(value = "/exp")
	public void exp(@RequestParam(value = "userId") Integer userId) {
	         OutputStream os;
			try {
				 User user = userService.getUserById(userId);
				 os = response.getOutputStream();
	             response.reset();// 娓呯┖杈撳嚭娴�  
	             response.setHeader("Content-disposition", "attachment; filename="+new String("缁熻鏁版嵁".getBytes("UTF-8"),"8859_1")+".xls");// 璁惧畾杈撳嚭鏂囦欢澶�   
	             response.setContentType("application/msexcel");// 瀹氫箟杈撳嚭绫诲瀷 
	             WritableWorkbook wwb = Workbook.createWorkbook(os); // 寤虹珛excel鏂囦欢
	             WritableSheet ws = wwb.createSheet("Sheet1", 1);   // 鍒涘缓涓�涓伐浣滆〃
	             ws.mergeCells(0, 0, 2, 2); 
	             ws.addCell(new Label(0, 0, "濮撳悕"));
	             ws.mergeCells(3, 3, 5, 2); 
	             ws.addCell(new Label(0, 2,"瀵嗙爜"));
	             String col1=user.getUserName();
	             String col2=user.getPassword();
	             ws.addCell(new Label(0, 1,col1));
	             ws.addCell(new Label(1, 1,col2));
	             wwb.write();
	             wwb.close();
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				// TODO Auto-generated finally block
			}   
	}
	@RequestMapping("/sendMessage")
	public void sendMessage(String message){
		producerService.sendMessage(message);
		ClassPathXmlApplicationContext springContext = SpringContainer.getSpringContext();
		Destination destination=(Destination)springContext.getBean("queueDestination");
		consumerService.receive(destination);
	}
}
