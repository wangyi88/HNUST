package com.cn.hnust.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.cn.hnust.util.JedisUtil;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * 本类是通过序列化 把文件放进redis 
 * @author 0
 *
 */
public class TestController {
		//序列化方法
	 	public static byte[] object2Bytes(Object value) {
	        if (value == null){
				return null;
			}
	        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
	        ObjectOutputStream outputStream;
	        try {
	            outputStream = new ObjectOutputStream(arrayOutputStream);
	            outputStream.writeObject(value);
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                arrayOutputStream.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	        return arrayOutputStream.toByteArray();
	    }
	 	//反序列化方法
	    public static Object byte2Object(byte[] bytes) {
	        if (bytes == null || bytes.length == 0){
				return null;
			}
	        try {
	            ObjectInputStream inputStream;
	            inputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
	            Object obj = inputStream.readObject();
	            return obj;
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        return null;
	    }
	    //保存文件方法
	    public static void setFile(String key,String path){
			  File fr = new File(path); 
			  JedisUtil.setByteValue(key.getBytes(), object2Bytes(fr));
	    }
	    //读取文件对象方法
	    public static File getFile(String key){
		     File file = (File)byte2Object(JedisUtil.getByteValue(key.getBytes()));
		     return file;
	    }
	    //从缓存中拿出数据并且把数据放进流中 导出为pdf文件
	    @SuppressWarnings("unused")
		public static void testFile(String key,String path)throws Exception{
	      setFile(key, path);
		  File file = getFile(key);
		  InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "gbk");
		  @SuppressWarnings("resource")
		  BufferedReader br = new BufferedReader(isr); 
		  String   record   =   null; 
		   // 1.新建document对象
           Document document = new Document();
           // 2.建立一个书写器(Writer)与document对象关联，通过书写器(Writer)可以将文档写入到磁盘中。
	       // 创建 PdfWriter 对象 第一个参数是对文档对象的引用，第二个参数是文件的实际名称，在该名称中还会给出其输出路径。
	       PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/test.pdf"));
	           // 3.打开文档
	       document.open();
	       //中文字体,解决中文不能显示问题
           BaseFont bfChinese = BaseFont.createFont("STSong-Light","UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
           //蓝色字体
           Font blueFont = new Font(bfChinese);
           blueFont.setColor(BaseColor.BLUE);
	       while ((record   =   br.readLine())   !=   null){ 
				  System.out.println("record:"+record);
				  // 4.添加一个内容段落
				  document.add(new Paragraph(record,blueFont));
		   }
           // 5.关闭文档
	       document.close();
	    }
	    
	    public static void main(String[] args) throws Exception{
	    	testFile("test", "D:\\data.xls");
	    }
	    
	    
	    
}
