package com.cn.hnust.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cn.hnust.pojo.User;
import com.cn.hnust.service.IUserService;

@Controller
@RequestMapping("/login")
public class LoginController {
	
	@Resource
	private IUserService userService;
	
	@RequestMapping("/toLogin")
	@ResponseBody
	public Map<String, Object> toLogin(HttpServletRequest request,Model model){
		User user=new User();
		user.setUserName(request.getParameter("userName"));
		user.setPassword(request.getParameter("password"));
		User userInfo=userService.selectByNamePass(user);
		Map<String, Object> map=new HashMap<String, Object>();
		if(userInfo!=null){
			request.getSession().setAttribute("user", userInfo);
			map.put("code", "200");
		}else{
			map.put("code", "400");
		}
		return map;
	}
}
